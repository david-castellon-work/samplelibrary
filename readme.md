# Sample Library Site

This is my run through of the mozilla sample library site demo.
I reskined all the pages with my own bootstrap sass theme, and
rewrote the code base in es6.

# Commands

```
   // builds the project
   npm run build
   // starts the project
   npm run start
