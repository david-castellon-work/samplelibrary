"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _mongoose = _interopRequireDefault(require("mongoose"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var Schema = _mongoose["default"].Schema;
var BookSchema = new Schema({
  title: {
    type: String,
    required: true
  },
  author: {
    type: Schema.ObjectId,
    ref: 'Author',
    required: true
  },
  summary: {
    type: String,
    required: true
  },
  isbn: {
    type: String,
    required: true
  },
  genre: [{
    type: Schema.ObjectId,
    ref: 'Genre'
  }]
});
BookSchema.virtual('url').get(function () {
  return '/samplelibrary/book/' + this._id;
});

var _default = _mongoose["default"].model('Book', BookSchema);

exports["default"] = _default;
//# sourceMappingURL=../maps/models/book.js.map
