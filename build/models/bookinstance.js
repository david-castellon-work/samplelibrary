"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _mongoose = _interopRequireDefault(require("mongoose"));

var _moment = _interopRequireDefault(require("moment"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var Schema = _mongoose["default"].Schema;
var BookInstanceSchema = new Schema({
  book: {
    type: Schema.ObjectId,
    ref: 'Book',
    required: true
  },
  imprint: {
    type: String,
    required: true
  },
  status: {
    type: String,
    required: true,
    "enum": ['Available', 'Maintenance', 'Loaned', 'Reserved'],
    "default": 'Maintenance'
  },
  due_back: {
    type: Date,
    "default": Date.now
  }
});
BookInstanceSchema.virtual('url').get(function () {
  return '/samplelibrary/bookinstance/' + this._id;
});
BookInstanceSchema.virtual('due_back_formatted').get(function () {
  return (0, _moment["default"])(this.due_back).format('MMMM Do, YYYY');
});
BookInstanceSchema.virtual('due_back_yyyy_mm_dd').get(function () {
  return (0, _moment["default"])(this.due_back).format('YYYY-MM-DD');
});

var _default = _mongoose["default"].model('BookInstance', BookInstanceSchema);

exports["default"] = _default;
//# sourceMappingURL=../maps/models/bookinstance.js.map
