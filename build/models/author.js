"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _mongoose = _interopRequireDefault(require("mongoose"));

var _moment = _interopRequireDefault(require("moment"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var Schema = _mongoose["default"].Schema;
var AuthorSchema = new Schema({
  first_name: {
    type: String,
    required: true,
    max: 100
  },
  family_name: {
    type: String,
    required: true,
    max: 100
  },
  date_of_birth: {
    type: Date
  },
  date_of_death: {
    type: Date
  }
});
AuthorSchema.virtual('name').get(function () {
  return this.family_name + ', ' + this.first_name;
});
AuthorSchema.virtual('url').get(function () {
  return '/samplelibrary/author/' + this._id;
});
AuthorSchema.virtual('lifespan').get(function () {
  var lifetime_string = '';

  if (this.date_of_birth) {
    lifetime_string = (0, _moment["default"])(this.date_of_birth).format('MMMM Do, YYYY');
  }

  lifetime_string += ' - ';

  if (this.date_of_death) {
    lifetime_string += (0, _moment["default"])(this.date_of_death).format('MMMM Do, YYYY');
  }

  return lifetime_string;
});
AuthorSchema.virtual('date_of_birth_yyyy_mm_dd').get(function () {
  return (0, _moment["default"])(this.date_of_birth).format('YYYY-MM-DD');
});
AuthorSchema.virtual('date_of_death_yyyy_mm_dd').get(function () {
  return (0, _moment["default"])(this.date_of_death).format('YYYY-MM-DD');
});

var _default = _mongoose["default"].model('Author', AuthorSchema);

exports["default"] = _default;
//# sourceMappingURL=../maps/models/author.js.map
