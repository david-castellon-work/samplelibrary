"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _mongoose = _interopRequireDefault(require("mongoose"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var Schema = _mongoose["default"].Schema;
var GenreSchema = new Schema({
  name: {
    type: String,
    required: true,
    min: 3,
    max: 100
  }
});
GenreSchema.virtual('url').get(function () {
  return '/samplelibrary/genre/' + this._id;
});

var _default = _mongoose["default"].model('Genre', GenreSchema);

exports["default"] = _default;
//# sourceMappingURL=../maps/models/genre.js.map
