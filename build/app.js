"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _express = _interopRequireDefault(require("express"));

var _path = _interopRequireDefault(require("path"));

var _mongoose = _interopRequireDefault(require("mongoose"));

var _bodyParser = _interopRequireDefault(require("body-parser"));

var _multer = _interopRequireDefault(require("multer"));

var _routes = _interopRequireDefault(require("./routes/routes"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var app = (0, _express["default"])(),
    upload = (0, _multer["default"])(); // const mongoDB = 'mongodb://159.65.191.124:27017/samplelibrary?retryWrites=true' ;

var mongoDB = 'mongodb+srv://dcastellon:Awesome0001@cluster0-0qm90.azure.mongodb.net/test?retryWrites=true';

_mongoose["default"].connect(mongoDB, {
  useNewUrlParser: true
});

var db = _mongoose["default"].connection;
db.on('error', console.error.bind(console, 'MongoDB connection error:'));
app.set('view engine', 'pug');
app.set('views', _path["default"].join(__dirname, 'views')); // static assets

app.use('/public', _express["default"]["static"](_path["default"].join(__dirname, 'public')));
app.use(_express["default"].urlencoded({
  extended: false
}));
app.use(_express["default"].json());
app.use(upload.array()); // routes

app.use(_routes["default"]);
var _default = app;
exports["default"] = _default;
//# sourceMappingURL=maps/app.js.map
