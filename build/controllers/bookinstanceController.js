"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _bookinstance = _interopRequireDefault(require("../models/bookinstance"));

var _book = _interopRequireDefault(require("../models/book"));

var _async = _interopRequireDefault(require("async"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var _require = require('express-validator/check'),
    body = _require.body,
    validationResult = _require.validationResult;

var _require2 = require('express-validator/filter'),
    sanitizeBody = _require2.sanitizeBody;

var bookinstance_controller = {}; // Display list of all BookInstances.

bookinstance_controller.bookinstance_list = function (req, res, next) {
  _bookinstance["default"].find().populate('book').exec(function (err, list_bookinstances) {
    if (err) {
      return next(err);
    } // Successful, so render.


    res.render('bookinstance_list', {
      title: 'Book Instance List',
      bookinstance_list: list_bookinstances
    });
  });
}; // Display detail page for a specific BookInstance.


bookinstance_controller.bookinstance_detail = function (req, res, next) {
  _bookinstance["default"].findById(req.params.id).populate('book').exec(function (err, bookinstance) {
    if (err) {
      return next(err);
    }

    if (bookinstance == null) {
      // No results.
      var err = new Error('Book copy not found');
      err.status = 404;
      return next(err);
    } // Successful, so render.


    res.render('bookinstance_detail', {
      title: 'Book:',
      bookinstance: bookinstance
    });
  });
}; // Display BookInstance create form on GET.


bookinstance_controller.bookinstance_create_get = function (req, res, next) {
  _book["default"].find({}, 'title').exec(function (err, books) {
    if (err) {
      return next(err);
    } // Successful, so render.


    res.render('bookinstance_form', {
      title: 'Create BookInstance',
      book_list: books
    });
  });
}; // Handle BookInstance create on POST.


bookinstance_controller.bookinstance_create_post = [// Validate fields.
body('book', 'Book must be specified').isLength({
  min: 1
}).trim(), body('imprint', 'Imprint must be specified').isLength({
  min: 1
}).trim(), body('due_back', 'Invalid date').optional({
  checkFalsy: true
}).isISO8601(), // Sanitize fields.
sanitizeBody('book').escape(), sanitizeBody('imprint').escape(), sanitizeBody('status').escape(), sanitizeBody('due_back').toDate(), // Process request after validation and sanitization.
function (req, res, next) {
  // Extract the validation errors from a request.
  var errors = validationResult(req); // Create a BookInstance object with escaped and trimmed data.

  var bookinstance = new _bookinstance["default"]({
    book: req.body.book,
    imprint: req.body.imprint,
    status: req.body.status,
    due_back: req.body.due_back
  });

  if (!errors.isEmpty()) {
    // There are errors. Render form again with sanitized values and error messages.
    _book["default"].find({}, 'title').exec(function (err, books) {
      if (err) {
        return next(err);
      } // Successful, so render.


      res.render('bookinstance_form', {
        title: 'Create BookInstance',
        book_list: books,
        selected_book: bookinstance.book._id,
        errors: errors.array(),
        bookinstance: bookinstance
      });
    });

    return;
  } else {
    // Data from form is valid
    bookinstance.save(function (err) {
      if (err) {
        return next(err);
      } // Successful - redirect to new record.


      res.redirect(bookinstance.url);
    });
  }
}]; // Display BookInstance delete form on GET.

bookinstance_controller.bookinstance_delete_get = function (req, res, next) {
  _bookinstance["default"].findById(req.params.id).populate('book').exec(function (err, bookinstance) {
    if (err) {
      return next(err);
    }

    if (bookinstance == null) {
      // No results.
      res.redirect('/catalog/bookinstances');
    } // Successful, so render.


    res.render('bookinstance_delete', {
      title: 'Delete BookInstance',
      bookinstance: bookinstance
    });
  });
}; // Handle BookInstance delete on POST.


bookinstance_controller.bookinstance_delete_post = function (req, res, next) {
  // Assume valid BookInstance id in field.
  _bookinstance["default"].findByIdAndRemove(req.body.id, function deleteBookInstance(err) {
    if (err) {
      return next(err);
    } // Success, so redirect to list of BookInstance items.


    res.redirect('/catalog/bookinstances');
  });
}; // Display BookInstance update form on GET.


bookinstance_controller.bookinstance_update_get = function (req, res, next) {
  // Get book, authors and genres for form.
  _async["default"].parallel({
    bookinstance: function bookinstance(callback) {
      _bookinstance["default"].findById(req.params.id).populate('book').exec(callback);
    },
    books: function books(callback) {
      _book["default"].find(callback);
    }
  }, function (err, results) {
    if (err) {
      return next(err);
    }

    if (results.bookinstance == null) {
      // No results.
      var err = new Error('Book copy not found');
      err.status = 404;
      return next(err);
    } // Success.


    res.render('bookinstance_form', {
      title: 'Update  BookInstance',
      book_list: results.books,
      selected_book: results.bookinstance.book._id,
      bookinstance: results.bookinstance
    });
  });
}; // Handle BookInstance update on POST.


bookinstance_controller.bookinstance_update_post = [// Validate fields.
body('book', 'Book must be specified').isLength({
  min: 1
}).trim(), body('imprint', 'Imprint must be specified').isLength({
  min: 1
}).trim(), body('due_back', 'Invalid date').optional({
  checkFalsy: true
}).isISO8601(), // Sanitize fields.
sanitizeBody('book').escape(), sanitizeBody('imprint').escape(), sanitizeBody('status').escape(), sanitizeBody('due_back').toDate(), // Process request after validation and sanitization.
function (req, res, next) {
  // Extract the validation errors from a request.
  var errors = validationResult(req); // Create a BookInstance object with escaped/trimmed data and current id.

  var bookinstance = new _bookinstance["default"]({
    book: req.body.book,
    imprint: req.body.imprint,
    status: req.body.status,
    due_back: req.body.due_back,
    _id: req.params.id
  });

  if (!errors.isEmpty()) {
    // There are errors so render the form again, passing sanitized values and errors.
    _book["default"].find({}, 'title').exec(function (err, books) {
      if (err) {
        return next(err);
      } // Successful, so render.


      res.render('bookinstance_form', {
        title: 'Update BookInstance',
        book_list: books,
        selected_book: bookinstance.book._id,
        errors: errors.array(),
        bookinstance: bookinstance
      });
    });

    return;
  } else {
    // Data from form is valid.
    _bookinstance["default"].findByIdAndUpdate(req.params.id, bookinstance, {}, function (err, thebookinstance) {
      if (err) {
        return next(err);
      } // Successful - redirect to detail page.


      res.redirect(thebookinstance.url);
    });
  }
}];
var _default = bookinstance_controller;
exports["default"] = _default;
//# sourceMappingURL=../maps/controllers/bookinstanceController.js.map
