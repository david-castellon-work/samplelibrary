"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _genre = _interopRequireDefault(require("../models/genre"));

var _book = _interopRequireDefault(require("../models/book"));

var _async = _interopRequireDefault(require("async"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var _require = require('express-validator/check'),
    body = _require.body,
    validationResult = _require.validationResult;

var _require2 = require('express-validator/filter'),
    sanitizeBody = _require2.sanitizeBody;

var genre_controller = {}; // Display list of all Genre.

genre_controller.genre_list = function (req, res, next) {
  _genre["default"].find().sort([['name', 'ascending']]).exec(function (err, list_genres) {
    if (err) {
      return next(err);
    } // Successful, so render.


    res.render('genre_list', {
      title: 'Genre List',
      genre_list: list_genres
    });
  });
}; // Display detail page for a specific Genre.


genre_controller.genre_detail = function (req, res, next) {
  _async["default"].parallel({
    genre: function genre(callback) {
      _genre["default"].findById(req.params.id).exec(callback);
    },
    genre_books: function genre_books(callback) {
      _book["default"].find({
        'genre': req.params.id
      }).exec(callback);
    }
  }, function (err, results) {
    if (err) {
      return next(err);
    }

    if (results.genre == null) {
      // No results.
      var err = new Error('Genre not found');
      err.status = 404;
      return next(err);
    } // Successful, so render.


    res.render('genre_detail', {
      title: 'Genre Detail',
      genre: results.genre,
      genre_books: results.genre_books
    });
  });
}; // Display Genre create form on GET.


genre_controller.genre_create_get = function (req, res, next) {
  res.render('genre_form', {
    title: 'Create Genre'
  });
}; // Handle Genre create on POST.


genre_controller.genre_create_post = [// Validate that the name field is not empty.
body('name', 'Genre name required').isLength({
  min: 1
}).trim(), // Sanitize (trim) the name field.
sanitizeBody('name').escape(), // Process request after validation and sanitization.
function (req, res, next) {
  // Extact the validation errors from a request.
  var errors = validationResult(req); // Create a genre object with escaped and trimmed data.

  var genre = new _genre["default"]({
    name: req.body.name
  });

  if (!errors.isEmpty()) {
    // There are errors. Render the form again with sanitized values/error messages.
    res.render('genre_form', {
      title: 'Create Genre',
      genre: genre,
      errors: errors.array()
    });
    return;
  } else {
    // Data from form is valid.
    // Check if Genre with same name already exists.
    _genre["default"].findOne({
      'name': req.body.name
    }).exec(function (err, found_genre) {
      if (err) {
        return next(err);
      }

      if (found_genre) {
        // Genre exists, redirect to its detail page.
        res.redirect(found_genre.url);
      } else {
        genre.save(function (err) {
          if (err) {
            return next(err);
          } // Genre saved. Redirect to genre detail page.


          res.redirect(genre.url);
        });
      }
    });
  }
}]; // Display Genre delete form on GET.

genre_controller.genre_delete_get = function (req, res, next) {
  _async["default"].parallel({
    genre: function genre(callback) {
      _genre["default"].findById(req.params.id).exec(callback);
    },
    genre_books: function genre_books(callback) {
      _book["default"].find({
        'genre': req.params.id
      }).exec(callback);
    }
  }, function (err, results) {
    if (err) {
      return next(err);
    }

    if (results.genre == null) {
      // No results.
      res.redirect('/catalog/genres');
    } // Successful, so render.


    res.render('genre_delete', {
      title: 'Delete Genre',
      genre: results.genre,
      genre_books: results.genre_books
    });
  });
}; // Handle Genre delete on POST.


genre_controller.genre_delete_post = function (req, res, next) {
  _async["default"].parallel({
    genre: function genre(callback) {
      _genre["default"].findById(req.params.id).exec(callback);
    },
    genre_books: function genre_books(callback) {
      _book["default"].find({
        'genre': req.params.id
      }).exec(callback);
    }
  }, function (err, results) {
    if (err) {
      return next(err);
    } // Success


    if (results.genre_books.length > 0) {
      // Genre has books. Render in same way as for GET route.
      res.render('genre_delete', {
        title: 'Delete Genre',
        genre: results.genre,
        genre_books: results.genre_books
      });
      return;
    } else {
      // Genre has no books. Delete object and redirect to the list of genres.
      _genre["default"].findByIdAndRemove(req.body.id, function deleteGenre(err) {
        if (err) {
          return next(err);
        } // Success - go to genres list.


        res.redirect('/catalog/genres');
      });
    }
  });
}; // Display Genre update form on GET.


genre_controller.genre_update_get = function (req, res, next) {
  _genre["default"].findById(req.params.id, function (err, genre) {
    if (err) {
      return next(err);
    }

    if (genre == null) {
      // No results.
      var err = new Error('Genre not found');
      err.status = 404;
      return next(err);
    } // Success.


    res.render('genre_form', {
      title: 'Update Genre',
      genre: genre
    });
  });
}; // Handle Genre update on POST.


genre_controller.genre_update_post = [// Validate that the name field is not empty.
body('name', 'Genre name required').isLength({
  min: 1
}).trim(), // Sanitize (escape) the name field.
sanitizeBody('name').escape(), // Process request after validation and sanitization.
function (req, res, next) {
  // Extract the validation errors from a request .
  var errors = validationResult(req); // Create a genre object with escaped and trimmed data (and the old id!)

  var genre = new _genre["default"]({
    name: req.body.name,
    _id: req.params.id
  });

  if (!errors.isEmpty()) {
    // There are errors. Render the form again with sanitized values and error messages.
    res.render('genre_form', {
      title: 'Update Genre',
      genre: genre,
      errors: errors.array()
    });
    return;
  } else {
    // Data from form is valid. Update the record.
    _genre["default"].findByIdAndUpdate(req.params.id, genre, {}, function (err, thegenre) {
      if (err) {
        return next(err);
      } // Successful - redirect to genre detail page.


      res.redirect(thegenre.url);
    });
  }
}];
var _default = genre_controller;
exports["default"] = _default;
//# sourceMappingURL=../maps/controllers/genreController.js.map
