"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _express = _interopRequireDefault(require("express"));

var _bookController = _interopRequireDefault(require("../controllers/bookController"));

var _authorController = _interopRequireDefault(require("../controllers/authorController"));

var _genreController = _interopRequireDefault(require("../controllers/genreController"));

var _bookinstanceController = _interopRequireDefault(require("../controllers/bookinstanceController"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var router = _express["default"].Router();

router.get('/', _bookController["default"].catalog);
router.get('/book/create', _bookController["default"].book_create_get);
router.post('/book/create', _bookController["default"].book_create_post);
router.get('/book/:id/delete', _bookController["default"].book_delete_get);
router.post('/book/:id/delete', _bookController["default"].book_delete_post);
router.get('/book/:id/update', _bookController["default"].book_update_get);
router.post('/book/:id/update', _bookController["default"].book_update_post);
router.get('/book/:id', _bookController["default"].book_detail);
router.get('/books', _bookController["default"].book_list);
router.get('/author/create', _authorController["default"].author_create_get);
router.post('/author/create', _authorController["default"].author_create_post);
router.get('/author/:id/delete', _authorController["default"].author_delete_get);
router.post('/author/:id/delete', _authorController["default"].author_delete_post);
router.get('/author/:id/update', _authorController["default"].author_update_get);
router.post('/author/:id/update', _authorController["default"].author_update_post);
router.get('/author/:id', _authorController["default"].author_detail);
router.get('/authors', _authorController["default"].author_list);
router.get('/genre/create', _genreController["default"].genre_create_get);
router.post('/genre/create', _genreController["default"].genre_create_post);
router.get('/genre/:id/delete', _genreController["default"].genre_delete_get);
router.post('/genre/:id/delete', _genreController["default"].genre_delete_post);
router.get('/genre/:id/update', _genreController["default"].genre_update_get);
router.post('/genre/:id/update', _genreController["default"].genre_update_post);
router.get('/genre/:id', _genreController["default"].genre_detail);
router.get('/genres', _genreController["default"].genre_list);
router.get('/bookinstance/create', _bookinstanceController["default"].bookinstance_create_get);
router.post('/bookinstance/create', _bookinstanceController["default"].bookinstance_create_post);
router.get('/bookinstance/:id/delete', _bookinstanceController["default"].bookinstance_delete_get);
router.post('/bookinstance/:id/delete', _bookinstanceController["default"].bookinstance_delete_post);
router.get('/bookinstance/:id/update', _bookinstanceController["default"].bookinstance_update_get);
router.post('/bookinstance/:id/update', _bookinstanceController["default"].bookinstance_update_post);
router.get('/bookinstance/:id', _bookinstanceController["default"].bookinstance_detail);
router.get('/bookinstances', _bookinstanceController["default"].bookinstance_list);
var _default = router;
exports["default"] = _default;
//# sourceMappingURL=../maps/routes/routes.js.map
