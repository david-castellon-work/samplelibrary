"use strict";

var _express = _interopRequireDefault(require("express"));

var _app = _interopRequireDefault(require("./app"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var server = (0, _express["default"])();
var port = process.env.PORT || '3000';
server.use('/samplelibrary', _app["default"]); // server . use ( '/samplelibrary' , app ) ;
// server . get ( '/' , samplelibrary_redirect ) ;
// server . get ( '/*' , samplelibrary_redirect ) ;

server.listen(port); // functions
// function samplelibrary_redirect ( req , res ) {
// 	res . redirect ( '/samplelibrary' ) ;
// }
//# sourceMappingURL=maps/server.js.map
