const gulp = require ( 'gulp' ) ;
const concat = require ( 'gulp-concat' ) ;
const sourcemaps = require ( 'gulp-sourcemaps' ) ;

// pug
// style plugins
const cssnano = require ( 'gulp-cssnano' ) ;
const sass = require ( 'gulp-sass' ) ( require ('node-sass') ) ;
const styleAliases = require ( 'gulp-style-aliases' ) ;
// javascript
const uglify = require ( 'gulp-uglify' ) ;
const babel = require ( 'gulp-babel' ) ;

gulp . task ( 'views' , function ( ) {
	return gulp . src ( 'src/views/*' )
	. pipe ( gulp . dest ( 'build/views' ) ) ;
} ) ;
gulp . task ( 'styles' , function ( ) {
	return gulp . src  ( 'node_modules/dark/src/dark.scss' )
	. pipe ( sourcemaps . init ( ) )
	. pipe ( sass ( ) . on ( 'error' , sass . logError ) )
	. pipe ( sourcemaps . write ( './maps' ) )
	. pipe ( gulp . dest ( 'build/public/styles' ) ) ;
} ) ;
gulp . task ( 'scripts' , function ( ) {
	return gulp . src ( 'src/**/*.js' )
	. pipe ( sourcemaps . init ( ) )
	. pipe ( babel ( {
		presets : [ '@babel/preset-env' ]
	} ) )
	. pipe ( sourcemaps . write ( './maps' ) )
	. pipe ( gulp . dest ( 'build' ) ) ;
} ) ;
gulp . task ( 'default' , gulp . series ( 'views' , 'styles' , 'scripts' ) ) ;
