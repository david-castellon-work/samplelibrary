import express from 'express' ;

import path from 'path' ;

import mongoose from 'mongoose' ;

import bodyParser from 'body-parser' ;
import multer from 'multer' ;

import routes from './routes/routes' ;

const app   	= express ( ) ,
      upload	= multer  ( ) ;

// const mongoDB = 'mongodb://159.65.191.124:27017/samplelibrary?retryWrites=true' ;
const mongoDB = 'mongodb+srv://dcastellon:Awesome0001@cluster0-0qm90.azure.mongodb.net/test?retryWrites=true' ;

mongoose . connect ( mongoDB , { useNewUrlParser : true } ) ;
var db = mongoose . connection ;
db . on ( 'error' , console . error . bind ( console , 'MongoDB connection error:' ) ) ;

app . set ( 'view engine' , 'pug' ) ;
app . set ( 'views' , path . join ( __dirname , 'views' , ) ) ; 

// static assets
app . use ( '/public' , express . static ( path . join ( __dirname , 'public' ) ) ) ;
app . use ( express . urlencoded ( { extended : false } ) ) ;
app . use ( express . json ( ) ) ;
app . use ( upload . array ( ) ) ;

// routes
app . use ( routes ) ;

export default app ;
