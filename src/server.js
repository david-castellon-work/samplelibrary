import express from 'express' ;
import app from './app' ;

var server = express ( ) ;
const port = process . env . PORT || '3000' ;

server . use ( '/samplelibrary', app ) ;
// server . use ( '/samplelibrary' , app ) ;
// server . get ( '/' , samplelibrary_redirect ) ;
// server . get ( '/*' , samplelibrary_redirect ) ;
server . listen ( port ) ;

// functions
// function samplelibrary_redirect ( req , res ) {
// 	res . redirect ( '/samplelibrary' ) ;
// }
