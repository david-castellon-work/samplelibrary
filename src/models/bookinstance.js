import mongoose from 'mongoose' ;
import moment from 'moment' ;

var Schema = mongoose.Schema;

var BookInstanceSchema = new Schema({
	book: { type: Schema.ObjectId, ref: 'Book', required: true },
	imprint: {type: String, required: true},
	status: {type: String, required: true, enum:['Available', 'Maintenance', 'Loaned', 'Reserved'], default:'Maintenance'},
	due_back: { type: Date, default: Date.now },
});

BookInstanceSchema.virtual('url')
	.get(function () {
		return '/samplelibrary/bookinstance/'+this._id;
	});


BookInstanceSchema.virtual('due_back_formatted')
	.get(function () {
		return moment(this.due_back).format('MMMM Do, YYYY');
	});

BookInstanceSchema.virtual('due_back_yyyy_mm_dd')
	.get(function () {
		return moment(this.due_back).format('YYYY-MM-DD');
	});

export default mongoose.model('BookInstance', BookInstanceSchema);
